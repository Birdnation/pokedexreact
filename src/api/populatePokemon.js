import axios from "axios";

export const PopulatePokemon = async (offset) => {
  let arrayPokemon = [];
  try {
    let result = await axios.get(
      "https://pokeapi.co/api/v2/pokemon?offset=" + offset + "&limit=20"
    );
    arrayPokemon = result.data.results;
    return arrayPokemon;
  } catch (e) {}

  // if success return value
  return null; // or set initial value
};
