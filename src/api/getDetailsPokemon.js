import axios from "axios";

export const GetDetailsPokemon = async (url) => {
  try {
    let result = await axios.get(url);
    return result;
  } catch (e) {}

  // if success return value
  return null; // or set initial value
};
