import { Button, Input } from "antd";
import React, { useState } from "react";
import { Card } from "react-bootstrap";
import "../assets/css/card-bg.css";

const Filter = ({ search }) => {
  const [searchId, setSearchId] = useState("");

  return (
    <Card className="card-find my-2">
      <Card.Body>
        <Card.Title>
          <i>Buscador</i>
        </Card.Title>

        <Input
          className="mb-3"
          allowClear
          onChange={(e) => setSearchId(e.target.value)}
          placeholder="Buscar por ID o nombre"
          style={{
            width: "100%",
          }}
        ></Input>
        <Button onClick={() => search(searchId)}>Buscar</Button>
      </Card.Body>
    </Card>
  );
};

export default Filter;
