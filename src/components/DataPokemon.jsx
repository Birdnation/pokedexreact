import { Table } from "antd";
import React from "react";
import { Container } from "react-bootstrap";
const DataPokemon = ({ height, weight, abilities, textPokemon }) => {
  let columns2 = [];
  let data2 = [{key: 0}];

  abilities.forEach((column, index) => {
    columns2.push({
      title: "habilidad " + (index + 1).toString(),
      dataIndex: "column" + index,
      key: index,
    });
    data2[0]["column" + index] = column;
  });


  const columns1 = [
    {
      title: "Altura",
      dataIndex: "height",
      key: "height",
    },
    {
      title: "Peso",
      dataIndex: "weight",
      key: "weight",
    },
  ];

  const data1 = [
    {
      key: "1",
      height: (height / 10).toString() + " m",
      weight: (weight / 10).toString() + " kg",
    },
  ];
  return (
    <Container className="ml-5 mr-5">
      <p style={{ fontWeight: "bolder" }}>Acerca de:</p>
      <p>
        {textPokemon}
      </p>
      <Table
        className="mx-5"
        columns={columns1}
        dataSource={data1}
        pagination={false}
      />
      <p className="my-3" style={{ fontWeight: "bolder" }}>
        Habilidades:
      </p>
      <Table
        className="mx-5 mb-3"
        columns={columns2}
        dataSource={data2}
        pagination={false}
      />
    </Container>
  );
};

export default DataPokemon;
