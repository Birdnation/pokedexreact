import React from 'react'
import { Container } from 'react-bootstrap'
import Logo from '../assets/img/pokeLogo.png'

const Header = ({children}) => {
  return (
    <>
      <Container className='justify-content-center text-center'>
        <img src={Logo} className="poke_logo" alt="" />
      </Container>
      {children}
    </>
  )
}

export default Header