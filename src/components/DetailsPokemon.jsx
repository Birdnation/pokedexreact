import { LoadingOutlined } from "@ant-design/icons";
import { Avatar, Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { GetDetailsPokemon } from "../api/getDetailsPokemon";
import { PokemonTypeColors } from "../utils/colors";
import BarChartVisx from "./BarChartVisx";
import DataPokemon from "./DataPokemon";
import PieChartVisx from "./PieChartVisx";

const DetailsPokemon = () => {
  const [isLoaded, setIsLoaded] = useState(true);
  const [color, setColor] = useState("");
  const [namePokemon, setNamePokemon] = useState("");
  const [descriptionPokemon, setDescriptionPokemon] = useState("");
  const [image, setImage] = useState();
  const [height, setHeight] = useState();
  const [weight, setWeight] = useState();
  const [abilities, setAbilities] = useState([]);
  const [genderRate, setGenderRate] = useState();
  const [statsBasePokemon, setStatsBasePokemon] = useState([]);
  const [textPokemon, setTextPokemon] = useState("");

  const { id } = useParams();

  useEffect(() => {
    GetDetailsPokemon("https://pokeapi.co/api/v2/pokemon/" + id).then((res) => {
      setColor(res.data.types[0].type.name);
      setNamePokemon(res.data.name);
      setHeight(res.data.height);
      setWeight(res.data.weight);
      setStatsBasePokemon(res.data.stats);

      if (res.data.sprites.other.dream_world.front_default) {
        setImage(res.data.sprites.other.dream_world.front_default);
      } else {
        setImage(res.data.sprites.front_default);
      }

      res.data.abilities.forEach((ability) => {
        GetDetailsPokemon(ability.ability.url).then((res) => {
          setAbilities((old) => [...old, res.data.names[5].name]);
        });
      });

      GetDetailsPokemon(res.data.species.url).then((res) => {
        setDescriptionPokemon(res.data.genera[5].genus);
        res.data.flavor_text_entries.forEach((text) => {
          if (text.language.name === "es") {
            setTextPokemon(text.flavor_text);
          }
        });
        setGenderRate(res.data.gender_rate);
        setIsLoaded(false);
      });
    });
  }, []);

  return (
    <Container>
      <Link
        style={{ textDecoration: "none", fontWeight: "bold", color: "black" }}
        to={".."}
      >
        {"<< Volver"}
      </Link>
      <div className="my-3 card__pokemon_detail">
        {isLoaded ? (
          <div className="text-center py-4">
            <LoadingOutlined
              style={{ fontSize: "6em", left: "auto", right: "auto" }}
            />
          </div>
        ) : (
          <Row>
            <Col
              lg="4"
              style={{
                backgroundColor: PokemonTypeColors[color].light,
                borderRadius: "16px",
                boxShadow: "box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1)",
              }}
            >
              <Row className="mx-3 my-3">
                <h4 style={{ fontWeight: "bolder", color: "white" }}>
                  #{id.padStart(4, "0")}
                </h4>
              </Row>
              <Row className="mx-3">
                <h3 style={{ fontWeight: "bolder", color: "white" }}>
                  {namePokemon.toUpperCase()}
                </h3>
              </Row>
              <Row className="justify-content-center my-4">
                <div style={{ width: "200px", height: "200px" }}>
                  <Avatar
                    style={{
                      padding: 10,
                      backgroundColor: PokemonTypeColors[color].medium,
                    }}
                    size={200}
                  />
                  <img
                    style={{
                      width: "200px",
                      height: "200px",
                      position: "absolute",
                      top: "8em",
                    }}
                    src={image}
                    alt=""
                  />
                </div>
              </Row>
              <Row
                className="justify-content-center my-4"
                style={{
                  backgroundColor: PokemonTypeColors[color].medium,
                  fontWeight: "bolder",
                  color: "white",
                }}
              >
                {descriptionPokemon}
              </Row>
            </Col>

            <Col lg="8">
              <Tabs
                defaultActiveKey="1"
                centered
                items={[
                  {
                    label: "Datos",
                    key: 1,
                    children: (
                      <DataPokemon
                        height={height}
                        weight={weight}
                        abilities={abilities}
                        textPokemon={textPokemon}
                      ></DataPokemon>
                    ),
                  },
                  {
                    label: "Estadísticas",
                    key: 2,
                    children: (
                      <Container className="my-4">
                        <p className="my-3" style={{ fontWeight: "bolder" }}>
                          Ratio de aparición por genero:
                        </p>
                        {genderRate !== -1 ? (
                          <Container className="text-center">
                            <PieChartVisx
                              genderRate={genderRate}
                            ></PieChartVisx>
                          </Container>
                        ) : (
                          <p className="my-3 text-center">Sin genero</p>
                        )}

                        <p className="my-3" style={{ fontWeight: "bolder" }}>
                          Estadísticas base:
                        </p>
                        <Container className="text-center">
                          <BarChartVisx
                            stats={statsBasePokemon}
                            barColor={PokemonTypeColors[color].medium}
                          ></BarChartVisx>
                        </Container>
                      </Container>
                    ),
                  },
                ]}
              />
            </Col>
          </Row>
        )}
      </div>
    </Container>
  );
};

export default DetailsPokemon;
