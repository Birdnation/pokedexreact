import { Card } from "antd";
import Meta from "antd/es/card/Meta";
import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { GetDetailsPokemon } from "../api/getDetailsPokemon";
import { LoadingOutlined } from "@ant-design/icons";

const CardPokemon = ({ pokemon }) => {
  const [image, setImage] = useState();
  const [numberPokemon, setNumberPokemon] = useState("");
  const [isLoaded, setIsLoaded] = useState(true);
  const [typeList, setTypeList] = useState([]);

  useEffect(() => {
    setIsLoaded(true);
    GetDetailsPokemon(pokemon.url).then((res) => {
      setNumberPokemon(res.data.id.toString());
      setTypeList(res.data.types);
      if (res.data.sprites.other.dream_world.front_default) {
        setImage(res.data.sprites.other.dream_world.front_default);
      } else {
        setImage(res.data.sprites.front_default);
      }
      setIsLoaded(false);
    });
  }, []);

  return (
    <>
      {isLoaded ? (
        <Card
          style={{
            width: "auto",
            position: "relative",
          }}
          className="my-2 card__pokemon"
          cover={
            <LoadingOutlined
              style={{ fontSize: "6em", left: "auto", top: "auto" }}
            />
          }
        ></Card>
      ) : (
        <a
          style={{ textDecoration: "none" }}
          href={"/pokemon/" + numberPokemon}
        >
          <Card
            style={{
              width: "auto",
              position: "relative",
            }}
            className="my-2 card__pokemon"
            cover={
              <img
                style={{ width: "120px", margin: "auto", height: "120px" }}
                className="my-3"
                alt="example"
                src={image}
              />
            }
          >
            <h3
              style={{
                position: "absolute",
                top: -0,
                fontSize: "1.5em",
                color: "rgba(0,0,0,0.5)",
                letterSpacing: "0.5em",
              }}
            >
              #{numberPokemon.padStart(3, "0")}
            </h3>
            <Meta title={pokemon.name.toUpperCase()} />

            <Row>
              {typeList.map((type, index) => (
                <Col key={index} xs lg="6">
                  <img
                    width={"100%"}
                    alt="Pokemon"
                    src={require(`../assets/img/TypePoke/${type.type.name}_es.png`)}
                  ></img>
                </Col>
              ))}
            </Row>
          </Card>
        </a>
      )}
    </>
  );
};

export default CardPokemon;
