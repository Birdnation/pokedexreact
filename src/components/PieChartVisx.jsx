import React, { useState } from "react";
import { Group } from "@visx/group";
import { Pie } from "@visx/shape";
import { Text } from "@visx/text";

const PieChartVisx = ({ genderRate }) => {
  const [active, setActive] = useState(null);
  const coins = [
    { symbol: "♂", amount: 100 - (genderRate / 8) * 100, color: "#0071CC" },
    { symbol: "♀", amount: (genderRate / 8) * 100, color: "#FF9999" },
  ];
  return (
    <svg width={200} height={200}>
      <Group top={100} left={100}>
        <Pie data={coins} pieValue={(data) => data.amount} outerRadius={100}>
          {(pie) => {
            return pie.arcs.map((arc) => {
              return (
                <g
                  key={arc.data.symbol}
                  onMouseEnter={() => setActive(arc.data)}
                  onMouseLeave={() => setActive(null)}
                >
                  <path d={pie.path(arc)} fill={arc.data.color}></path>
                </g>
              );
            });
          }}
        </Pie>
        {active ? (
          <>
            <Text textAnchor="middle" fill="#fff" fontSize={20} dy={-20}>
              {`${Math.floor(active.amount)} %`}
            </Text>

            <Text textAnchor="middle" fill="#fff" fontSize={40} dy={20}>
              {active.symbol}
            </Text>
          </>
        ) : (
          <>
            <Text textAnchor="middle" fill="#fff" fontSize={20} dy={0}>
              % de aparición
            </Text>
          </>
        )}
      </Group>
    </svg>
  );
};

export default PieChartVisx;
