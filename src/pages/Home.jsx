import { Pagination } from "antd";
import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import CardPokemon from "../components/CardPokemon";
import Filter from "../components/Filter";
import { PopulatePokemon } from "../api/populatePokemon";
import { GetDetailsPokemon } from "../api/getDetailsPokemon";

const Home = () => {
  const [searchActive, setSearchActive] = useState(false);
  const [current, setCurrent] = useState(1);
  const [listPokemon, setListPokemon] = useState([]);
  const onChange = (page) => {
    setCurrent(page);
  };

  const search = (numberOrName) => {
    console.log(numberOrName);
    if (numberOrName === "") {
      setSearchActive(false);
    } else {
      setSearchActive(true);
      setListPokemon([]);
      GetDetailsPokemon(
        "https://pokeapi.co/api/v2/pokemon/" + numberOrName
      ).then((res) =>
        {try {
          setListPokemon([{ name: res.data.name, url: res.request.responseURL }])
        } catch (error) {
          setListPokemon([]);
        }}
      );
    }
  };

  useEffect(() => {
    setListPokemon([]);
    if (!searchActive) {
      PopulatePokemon((current - 1) * 20).then((res) => setListPokemon(res));
    }
  }, [current, searchActive]);

  return (
    <div>
      <Container className="mt-4">
        <Row>
          <Col lg="3">
            <Filter search={search}></Filter>
          </Col>
          <Col>
            <Container>
              <Row>
                {listPokemon.length > 0 ? (listPokemon.map((pokemon, index) => (
                  <Col key={index} xs lg="3">
                    <CardPokemon pokemon={pokemon}></CardPokemon>
                  </Col>
                ))) : <h3 className="text-center">Sin datos</h3>}
              </Row>
              {searchActive ? null : (
                <Container className="text-center my-4 py-4">
                  <Pagination
                    style={{ fontWeight: "bold" }}
                    current={current}
                    onChange={onChange}
                    total={1154}
                    defaultPageSize={20}
                    showSizeChanger={false}
                  />
                </Container>
              )}
            </Container>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Home;
