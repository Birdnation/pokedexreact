import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import DetailsPokemon from './components/DetailsPokemon';
import Header from './components/Header';
import Home from './pages/Home';

function App() {
  return (
    <div className="App">
      <Header>
        <BrowserRouter>
          <Routes>
            <Route index element={<Home></Home>}></Route>
            <Route path='/pokemon/:id' element={<DetailsPokemon></DetailsPokemon>}></Route>
          </Routes>
        </BrowserRouter>
      </Header>

    </div>
  );
}

export default App;
